<?php
class Encrypter
{
private $cipher = "aes-128-cbc";
private $IBLOCK_ID = 4;

 public function Crypt($text, $pass)
 {
     $shifr = bin2hex(openssl_random_pseudo_bytes(3));
     $encr = openssl_encrypt($text, $this->cipher , $pass);
     CModule::IncludeModule("iblock");
     $arLoadProductArray = Array(
         "IBLOCK_ID" => $this->IBLOCK_ID,
         "NAME" => "Сообщение",
         "DETAIL_TEXT" => "",
         "PROPERTY_VALUES"=> array(
             "PASSWORD" => $pass,
             "MASSAGE" => $encr,
             "SHIFR" => $shifr
         ),
     );
     $el = new CIBlockElement;
     $el->Add($arLoadProductArray);
     $url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/'.$shifr;
     return 'Ссылка: '.$url;
 }

 public function GetMassage($pass) {

     CModule::IncludeModule("iblock");
     $url =$_SERVER ['HTTP_REFERER'];
     $shifr = substr($url, -6);
    $arSelect = Array("ID","PROPERTY_PASSWORD","PROPERTY_SHIFR","PROPERTY_MASSAGE");
    $arFilter = Array("IBLOCK_ID"=>$this->IBLOCK_ID, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, 0, $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        if($arFields['PROPERTY_SHIFR_VALUE']==$shifr){
            if($arFields['PROPERTY_PASSWORD_VALUE']==$pass){
                $original_plaintext = openssl_decrypt($arFields['PROPERTY_MASSAGE_VALUE'], $this->cipher, $pass);
            }else{
                echo 'Неправельный пароль';
            }
        }
    }
    return $original_plaintext;
 }
    public function DeleteMassage($pass){

        CModule::IncludeModule("iblock");
        $url =$_SERVER ['HTTP_REFERER'];
        $shifr = substr($url, -6);
        $arSelect = Array("ID","PROPERTY_PASSWORD","PROPERTY_SHIFR","PROPERTY_MASSAGE");
        $arFilter = Array("IBLOCK_ID"=>$this->IBLOCK_ID, "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, 0, $arSelect);
        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            if($arFields['PROPERTY_SHIFR_VALUE']==$shifr){
                if($arFields['PROPERTY_PASSWORD_VALUE']==$pass){
                    CIBlockElement::Delete($arFields['ID']);
                    echo 'Сообщение удалено';
                }else{
                    echo 'Неправельный пароль';
                }
            }
        }
    }
}