<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => "Шифрование",
    "DESCRIPTION" => "Производим шифрование информации пользователя",
    "PATH" => array(
        "ID" => "crypt",
    ),
);
