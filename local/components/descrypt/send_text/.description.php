<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => "Расшифровка",
    "DESCRIPTION" => "Производим расшифровку информации пользователя",
    "PATH" => array(
        "ID" => "descrypt",
    ),
);
