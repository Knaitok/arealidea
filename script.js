var id;
var formData = new FormData();
var servText = document.querySelector('#block');
document.querySelector('#Encrypt_btn').addEventListener('click', function(e){
    id = e.target.id;
    formData.append('id',id);
    formData.append('pass',document.forms.crypt.pass.value);
    formData.append('text',document.forms.crypt.text.value);
    e.preventDefault();
    var xhr = new XMLHttpRequest();

    xhr.open('POST', 'local/lib/encrypt.php');

    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4 && xhr.status === 200){
            servText.textContent = xhr.responseText;
        }
    }
    xhr.send(formData);
});

